# Release Notes
In this release, I have made significant updates to README.md

1. [Documentation](Documentation.md)
1. [Example](Example)
1. [Copilot](https://aws.github.io/copilot-cli/)

Some notes for Datica Customers migrating to ClearDATA and using AWS Copilot to replace DCC functionality.
ClearDATA uses auto-remediation to fix issues that are found that would put Compliance at risk.  Unfortunately, that can cause issues with an Infrastructure as Code tool such as AWS Copilot.  With this in mind, there are a couple of things to keep in mind.
  1. All application names must begin with "cdco-" this ensures that the permissions needed to run copilot stay in place.
  1. AWS Copilot can configure VPCs, subnets, load balancers, target groups, etc. ClearDATA currently defines those resources using their tooling to ensure, deployment in a compliant fashion.  You will find instructions in the Migration Docum. With this rule in mind, most of your services will be defined as "Backend Services."  You could also potentially use "Worker Services" however in AWS Copilot a worker service works with AWS SQS and I have not been aware of any customer using SQS.
  1. A special role has been created to use while running AWS Copilot.  The role name is "{+copilot-role+}" it is no longer {-ClearDATA-copilot-} because of a conflict with other naming conventions.  You will need to assume that role in order successfully use AWS Copilot. The "CLI and MFA and Roles" section of [Documentation](Documentation.md) contains updated detailed instructions.
  1. If you need to use "Load Balanced Services" refer to the section "A special tag"
  1. If copilot is working, but then fails with permission issues, check the section "STS Role timeout"
  1. Added a note about enabling specific version of TLS.
  1. Added a note about the need to get a security exception from ClearDATA.
  1. Added a note about a utility to clean up and remove tags where it is not needed. 

I am adding an example application.  Several customers use Mirth Connect from Nextgen Health.  I am including in the example directory structure and manifest files.  It includes a primary Postgres database, a read-only Postgres database, 2 Mirth connect services and a ProFTPD service.  You will be able to use this as a guide and potentially try it out.

# Hints
Using a scale of zero will help you to get your service deployed into a pipeline easier.  Just the same as in DCC, it would take a long time for a service to deploy and would fail if the container could not start.  I like to deploy with a scale of zero and then change the code and submit it to the repo to troubleshoot.  I believe that this workflow is faster and less burdensome.
