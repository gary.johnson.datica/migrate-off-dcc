# Migrate-off-DCC

Datica no longer supports the DCC platform. It is time to use a new tool to help your infrastructure deployment. ClearDATA is the primary option for former Datica Customers. Following are ClearDATA disclaimers and instructions for how to use AWS Copilot to replace the DCC infrastructure tool. This repo is public to allow easy access by former Datica Customers. It is also publicly posted to enable customers to pull and propose modifications and updates. All Datica customers have found themselves in a difficult position, and if you would like to help others with hints and tricks you have discovered, please share them here.

# ClearDATA Positioning on AWS Copilot
ClearDATA's mission is to help customers deploy their applications on securely configured Cloud solutions provided by AWS, Azure, and Google Cloud. ClearDATA provides that support with no opinion on the application pattern used to deploy your application.

Datica provided an opinionated solution of the pattern to use when deploying your application. Datica pre-selected an easy path to deploy your application. Datica provided a tool called "DCC, short for Datica Cloud Compliance," to help customers deploy applications within the opinionated pattern.

ClearDATA is a proponent of DevOps processes that encourage practices allowing the customer to be fast and agile in their development and deployment workflows. That enables the customer to deploy new versions of their applications that can be tested and verified before deployment. That allows for incremental changes with a high degree of confidence that those changes will not negatively impact their customers.

With that in mind, ClearDATA recognizes and appreciates tools that help customers deploy their applications into ClearDATA-managed cloud infrastructure. Some examples may be AWS Copilot, Terraform, CloudFormation, Amazon ECS CLI, CDK, and Pulumi.

This document is not a recommendation or edict that former Datica customers use AWS Copilot. It is presented as an option for the customer. AWS Copilot is very different from DCC. However, the core tenants of making deployment of ECS using Fargate are at the heart of both toolsets.

The most significant difference between the tools is the User Interface. As a web-based interface, DCC makes it easy to pop into a customer solution from any location with a web browser. AWS copilot as a command-line interface is tied to a computer system with the appropriate tooling installed and configured. And with your code installed within a directory structure.

The ability of ClearDATA to directly assist in a customer Copilot deployment and to support an AWS copilot workflow is minimal. This means a couple of things. 

1. The customer will need to engage with the AWS Copilot Documentation and the AWS Copilot project team for assistance, not ClearDATA. 
1. Some functionality of AWS Copilot needs to be overridden to be used in a ClearDATA managed AWS account, most notably VPC configuration and setup.

Earlier, other tools were mentioned besides AWS Copilot, such as Amazon ECS CLI, Terraform, Pulumi, and CloudFormation. The same would be said about ClearDATA support for those tools. ClearDATA will provide best-effort support for those products in trying to help the customer, but issues and failures of those products will not be escalated. They will be low-priority work for ClearDATA support personnel. If the support engineer has no experience or knowledge of the product, they may decline to provide support.
## Other Options
- The first option is to engage the ClearDATA Professional Services team.
- The second option is that all your services can be deployed using AWS console or AWS CLI. ClearDATA support personnel are available to assist the customer with using those tools.
- The third option is Amazon ECS CLI. The exciting feature is that if you use docker-compose files to build your application locally, you can leverage a docker-compose file to deploy into ECS.  
# Overview of AWS Copilot
AWS Copilot is an open-source project sponsored by AWS. AWS Copilot is a command-line interface (CLI) that enables customers to quickly launch and easily manage containerized applications on AWS. AWS Copilot provides a simple declarative set of commands, including examples and guided experiences built to help customers deploy soon.

There are several different ways to use and run AWS Copilot. This guide will try to limit and reduce the options in favor of practicality and making your application work.

## What to keep from DCC to minimize the impact of change
To speed migration, it is logical to keep some DCC infrastructure in place, at least for the production environment, to reduce the effect of losing DCC infrastructure. The VPC (network, subnets, etc.) plus resources such as EFS, RDS, etc.
## Preliminary Steps
The following should be completed before starting to work with AWS Copilot.
Get AWS IAM User - API Access Key and Secret Access Key.
This step is required and will need to be completed by ClearDATA personnel. An AWS IAM User is necessary because the Datica AWS SSO support will disappear. In addition, the users need permission to switch roles and to use an Admin role with Boundary Permissions.
### Install AWS CLI

#### Linux Commands
 ```
echo "Update AWS CLI to version 2.x"
echo
sudo rm -rf /usr/local/aws-cli
sudo rm -rf /usr/local/bin/aws
sudo rm -rf /usr/bin/aws
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
aws --version
rm awscliv2.zip
```

### Install JQ

#### Linux Commands
```
echo
echo "Install jq version 1.6."
wget -O jq https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64
chmod +x ./jq
sudo cp jq /usr/bin
```

### Install Session Manager Plugin

#### Linux Commands
```
session-manager-plugin
if [[ $? != 0 ]]; then
echo
echo "Install Session manager plugin"
curl "https://s3.amazonaws.com/session-manager-downloads/plugin/latest/linux_64bit/session-manager-plugin.rpm" -o "session-manager-plugin.rpm"
sudo yum install -y session-manager-plugin.rpm
fi
```
 
### Install AWS Copilot
https://aws.github.io/copilot-cli/docs/getting-started/install/ Be sure to get the latest version.  Or at least v1.20.0.  This is the first version that includes a manifest file for each environment.  This will be very helpful.

### Configure AWS CLI Profile
If using an IAM user, the command will be `aws configure –profile`
If using SSO, the command will be `aws configure sso –profile`
### export AWS_PROFILE=
AWS CLI uses the `default` profile as the default. However, if you create an environment variable of `AWS_PROFILE=`, it will use the named profile rather than the "default" profile.
With most AWS CLI commands, you can specify `–-profile` as an additional flag to override the default or the one specified in the environment variable.
### CLI and MFA and Roles
The initial challenge will be to learn how to get a valid session ID to use in your Command-Line session. You need to have admin access to run the copilot commands. You need to use MFA to get copilot access and then change your role to `copilot-role`. Some customers have found a bash script that helps to get MFA access. It works using a wrapper function in .bashrc that calls the [MFA script](https://github.com/asagage/aws-mfa-script).
The MFA script gets you logged in with MFA. Next, you need a session token that identifies you as using an admin role.  
Use the following command line argument to get a session token. Substitute <Account_ID> with your AWS Account_ID,  with an email address, and `--profile default` with the profile id that you have defined in AWS credentials.  The `--profile` name "copilot" can be changed to whatever you prefer. Just be aware that this command uses several times, and all occurrences should be changed. The last thing to know is that this --profile name is then used with copilot.  
```
aws sts assume-role --role-arn arn:aws:iam::<Account_ID>:role/copilot-role --role-session-name "<email>-copilot" --profile default | jq -r '.Credentials | "aws configure set aws_session_token \(.SessionToken) --profile copilot\naws configure set aws_access_key_id \(.AccessKeyId) --profile copilot\naws configure set aws_secret_access_key \(.SecretAccessKey) --profile copilot\n"' | sh && aws configure set region us-east-1 --profile copilot && export AWS_PROFILE=copilot
```
The above command gets temporary credentials to use the role copilot-role.  It then updates the AWS config and credentials found in your $HOME/.aws/config file. It also sets an environment variable that will override your default config.  The credentials will work for about an hour so if you find that you no longer have permissions, just run the above command with your changes again.  You can run it in another CLI session if your current CLI session is active.  

Then verify that the role you are using is indeed the "copilot-role" role.  Run the following command.
`aws sts get-caller-identity`

After running the previous command check the ARN for "copilot-role" similar to the following example.
```
{
    "UserId": "AROA3OKMEY2POAF6MZRFS:gary.johnson@cleardata.com-copilot",
    "Account": "786xxx5826xx",
    "Arn": "arn:aws:sts::786xxx5826xx:assumed-role/copilot-role/gary.johnson@cleardata.com-copilot"
}
```

You can also verify that the profile is correct by using the following and specifying the profile to use.
`aws sts get-caller-identity --profile copilot`
If you get the expected results this time then check your environment variables. Use this command.
`printenv | grep AWS`  
The results should be only one variable, AWS_PROFILE.

### STS Role timeout
I have experienced errors while AWS Copilot is running. It is usually in the form of a permission error that some user or role does not have the permission needed to assume the required role.  I have found this is because the assumed "copilot-role" role has timed out.  I have created a small shell script that includes the above and runs every 15 minutes.  It will updates the credentials for the assumed role every 15 minutes.  Following is the script refreshtoken.sh.
```
#!/usr/bin/env bash

accountId=xxxxxxxx
emailaddress=gary.johnson@cleardata.com
profile=default
copilotProfile=copilot

i=0
while [ $i -ne 10 ]
do
        i=$(($i+1))
        aws sts assume-role --role-arn arn:aws:iam::${accountId}:role/copilot-role --role-session-name "${emailaddress}-copilot" --profile ${profile} | jq -r '.Credentials | "aws configure set aws_session_token \(.SessionToken) --profile ${copilotProfile}\naws configure set aws_access_key_id \(.AccessKeyId) --profile ${copilotProfile}\naws configure set aws_secret_access_key \(.SecretAccessKey) --profile ${copilotProfile}\n"' | sh && aws configure set region us-east-1 --profile ${copilotProfile} && export AWS_PROFILE=${copilotProfile}
        echo "This is step $i of 10.  Sleeping for 900 Seconds"
        sleep 900
done
```
You can use this a couple of ways.  One way is to open another CLI window and run it in that window.  It updates the credentials file at $HOME/.aws/credentials.  It does that by using the command `aws configure set ....`  The other way you can use it is to run it in the background.  To do that you append an ampersand sign to the end of the command, assuming you called your script refreshtoken.sh you would put the following on the Command Line `./refreshtoken.sh &` This will run the command in the background and about every 15 minutes you get a message that it has refreshed the token. You may notice this command does export to AWS_Profile.  However, since it is happening in the background or alternative CLI session it will not help, be sure to `export AWS_PROFILE=copilot` in the CLI session you are running copilot commands in. 


### Create a Directory for Application
This directory will become the root directory of your application. In the next step, we will clone your current service repo into a subdirectory.  You will run all copilot commands in each service directory. AWS copilot refers to this as a workspace that will become important at the time of creating pipelines. 

#### Add a subdirectory for each service.
Next, you will create a subdirectory for each service.  You will `git clone` your service repository to create a subdirectory within the application directory. 
If you have code services that were not created by a pipeline, either Datica created that code service for you, or you created the code service directly using an image and adding files if needed.  Go ahead and create a subdirectory for that service.  A Dockerfile will need to be created in that directory with the information from the code service.
If you are using Cloud Native Buildpacks, we will create a pipeline and then modify the buildspec file to handle a Cloud Native Buildpack.

### Cloud-Native Builds
As previously mentioned, AWS Copilot does not currently include a feature to run Cloud-Native BuildPacks natively. However, much like DCC the buildspec file can be modified to use buildpacks rather than a Dockerfile.
Following is some AWS Documentation on how to use Cloud Native Buildpacks in AWS. "Creating container images with Cloud Native Buildpacks using AWS CodeBuild and AWS CodePipeline."
The key item to extract from the previous blog is the [buildspec](https://github.com/aws-containers/codepipeline-codebuild-buildpacks-example) file.
An example from the blog follows.  Notice this example uses the Paketo Build Packs.  Datica used the Heroku build packs.  You may want to try the Paketo Build Packs, what you use can be changed in the "env: variables:" section.
```
          version: 0.2
          env:
            variables:
              builder: "paketobuildpacks/builder:base"
              application_name: "buildpacks-test"
              pack_version: "0.18.1"
            exported-variables:
            # Exported the image tag to be used later in the CodePipeline
            - IMAGE_TAG
          phases:
            install:
              commands:
              # Download the pack linux binary
              - wget -q https://github.com/buildpacks/pack/releases/download/v$pack_version/pack-v$pack_version-linux.tgz -O - | tar -xz
              - chmod +x ./pack
            pre_build:
              commands:
              # Log in to ECR
              - ECR_DOMAIN="$AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com"
              - aws ecr get-login-password --region $AWS_DEFAULT_REGION | docker login --username AWS --password-stdin $ECR_DOMAIN
              # Set up some derived values for subsequent phases
              - COMMIT_HASH=$(echo $CODEBUILD_RESOLVED_SOURCE_VERSION | cut -c 1-7)
              - ECR_REPOSITORY="$ECR_DOMAIN/$application_name"
              - IMAGE_TAG="$ECR_REPOSITORY:$COMMIT_HASH"
            build:
              commands:
              - |
                ./pack build --no-color --builder $builder \
                --cache-image $ECR_REPOSITORY:cache \
                --tag $IMAGE_TAG $ECR_REPOSITORY:latest \
                --publish
```
Here is another example of a buildspec file.  This is the buildspec file that DCC used.  It allowed for either a DockerFile or a Cloud Native Build. You will need to modify the following buildspec.  It can be simplified from the current definition.  The other capability that the following buildspec file has is it includes all of the defined environment variables in the build process.  
```
{
  "version": "0.2",
  "env": {
    "exported-variables": [
      "CommitId"
    ]
  },
  "phases": {
    "pre_build": {
      "commands": [
        "echo \"Logging into ECR\"",
        "$(aws ecr get-login --no-include-email --region $AWS_DEFAULT_REGION)",
        "docker pull $IMAGE_REPO:latest || True",
        "echo \"Preparing build env\"",
        "touch envfilenoquotes && > envfilenoquotes",
        "touch envfile && > envfile",
        "buildArgs=",
        "IFS=\"\n\"",
        "for param in $(aws ssm get-parameters-by-path --path /dcc/development/nate-test/codeservice/frontend/env --output json | jq -cr \".Parameters[]\"); do key=$(echo $param | jq -cr \".Name\") && value=$(echo $param | jq -cr \".Value\") && keyName=${key//\"/dcc/development/nate-test/codeservice/frontend/env/\"/\"\"} && buildArgs=\"${buildArgs}--build-arg ${keyName} \" && echo $keyName=\"'\"$value\"'\" >> envfile && echo $keyName=$value >> envfilenoquotes; done",
        "for param in $(aws ssm get-parameters-by-path --path /dcc/development/nate-test/codeservice/frontend/secrets --output json | jq -cr \".Parameters[]\"); do key=$(echo $param | jq -cr \".Name\") && arn=$(echo $param | jq -cr \".Value\") && keyName=${key//\"/dcc/development/nate-test/codeservice/frontend/secrets/\"/\"\"} && value=$(aws secretsmanager get-secret-value --secret-id=${arn} --output json | jq -cr \".SecretString\") && buildArgs=\"${buildArgs}--build-arg ${keyName} \" && echo $keyName=\"'\"$value\"'\" >> envfile && echo $keyName=$value >> envfilenoquotes; done",
        "IFS=\" \"",
        "stack=$(cat envfilenoquotes | { grep STACK || :; })",
        "stackVersion=${stack: -2}",
        "herokuVersion=${stackVersion:-20}",
        "if [[ -z $DOCKERFILE ]]; then echo \"Using heroku v$herokuVersion\"; fi",
        "if [[ -z $DOCKERFILE ]]; then echo \"Downloading BuildPacks\"; fi",
        "if [[ -z $DOCKERFILE ]]; then curl -sSL \"https://github.com/buildpacks/pack/releases/download/v0.21.1/pack-v0.21.1-linux.tgz\" | tar -C /usr/local/bin/ --no-same-owner -xzv pack; fi",
        "echo \"Executing pre-build commands...\"",
        "eval $(egrep -v \"^#\" envfile | xargs -0) "
      ]
    },
    "build": {
      "commands": [
        "echo \"Build starting\"",
        "echo \"The CODEBUILD_RESOLVED_SOURCE_VERSION is ----> $CODEBUILD_RESOLVED_SOURCE_VERSION \"",
        "if [[ ! -z $DOCKERFILE ]]; then eval $(egrep -v \"^#\" envfile | xargs -0) docker build -t $IMAGE_REPO:latest -t $IMAGE_REPO:$CODEBUILD_RESOLVED_SOURCE_VERSION --build-arg BUILDKIT_INLINE_CACHE=1 --cache-from $IMAGE_REPO:latest -f ./$SERVICE_PATH/$DOCKERFILE $buildArgs ./$SERVICE_PATH; fi",
        "if [[ -z $DOCKERFILE ]]; then pack build $IMAGE_REPO:latest -v -p ./$SERVICE_PATH --cache-image $IMAGE_REPO:cache -B heroku/buildpacks:$herokuVersion -D $SERVICE_PROCESS --publish --env-file ./envfilenoquotes; fi",
        "echo \"Build completed\"",
        "export CommitId=$CODEBUILD_RESOLVED_SOURCE_VERSION"
      ]
    },
    "post_build": {
      "commands": [
        "echo \"Pushing to ECR\"",
        "if [[ -z $DOCKERFILE ]]; then docker pull docker pull $IMAGE_REPO:latest; docker tag docker pull $IMAGE_REPO:latest $IMAGE_REPO:$CODEBUILD_RESOLVED_SOURCE_VERSION; fi",
        "if [[ ! -z $DOCKERFILE ]]; then docker push $IMAGE_REPO:latest; fi",
        "docker push $IMAGE_REPO:$CODEBUILD_RESOLVED_SOURCE_VERSION",
        "echo \"Image pushed\"",
        "echo \"Executing post-build commands...\"",
        "eval $(egrep -v \"^#\" envfile | xargs -0) "
      ]
    }
  }
}
```


### Copilot Steps
As mentioned earlier, this is a step-by-step guide to help facilitate an easy transition from DCC managed application to an AWS Copilot-managed application. The following steps will help ensure success in migrating your application from DCC onto Copilot.  The important concept to remember is that you will run the copilot app init multiple times.  Once per workspace, which will probably be per service.  You can however have multiple services within a workspace if you would like.  If you have multiple workspaces, every time you run `copilot app init` you want to specify the same workspace so that they all work together.
``` 
Copilot app init (–resource-tags and or –domain example.com)
```
This command is run at the root directory of the workspace. 
–resource-tags will ensure all resources created include tags that can be used for identification, application logic, and billing purposes.
#### A special tag
ClearDATA’S safeguards will auto-remediate ALB and ALB target groups if encryption in flight is not enabled. If you’re unable to update your application to enable encryption in flight please work with ClearDATA to execute a security exception before moving forward with this tag

If you are deploying in copilot "Load Balanced Services" you will need to add a tag when you create the app.  When using the command `copilot app init` add the --resource-tags flag.  The special tag is `cleardata:remediation-disabled=true`  An example of how this looks in your command is `copilot app init cdco-example --resource-tags "cleardata:remediation-disabled=true,second-tag=example`  This example has two tags attached.  This command will apply this tag to all resources created.  This is primarily for target groups that are created.  ClearDATA CyberHealth will report the target groups as non-compliant and remove them.  Flagging them like this allows you to fix them and then either set the tag to false or remove the tag.  

In the "utilities" directory of this repository is a python script to remove the tag cleardata:remediation-disabled=true from resources that are not an NLB, an ALB, or a target group.  Keeping them in place would allow you to deploy resources that are non-compliant and defeats the purpose of having ClearDATA as a partner. 

There is a capability to tag resources at the time you deploy a service, however, this may not work because the ALB and NLB are created as part of the environment and therefore will not receive the tags in time to protect them.

```
copilot env init –container-insights –import-cert-arns 
```
This step is essential to import the VPC and other networking resources. With v1.20.0 it can be defined in the manifest.  Before v1.20.0 of copilot, it can only be defined at the moment the INIT environment is run.
- Provide environment name
  This is of the environment. Copilot allows for multiple environments such as test, acceptance, and production. Copilot also provides for using git branches for those different environments. Enabling you to introduce new code in test and development environments and then promoting to varying branches as needed.
- Choose Profile
  The profile is the AWS CLI profile. If you do not pass the profile as a flag, it will prompt you to provide the profile.
- Select the VPC_ID of the DCC APP
  You can specify the VPC_ID that you want to use as a flag. Or you can select the VPC using the prompts provided by the CLI.
- Select all Public Subnets
  DCC does not deploy ECS resources into public subnets. Copilot, by default, deploys internet-facing services in the public subnets. Select all private subnets.
  Special care must be used to deploy resources on the private subnets.
- Select all Private Subnets
  The copilot cli will provide the opportunity to choose the private subnets to use. Select all available private subnets.
- –import-cert-arns
  Flag to import the ARNs of your existing private certificates.
- –internal-alb-allow-vpc-ingress
  If you'd like your ALB to receive ingress traffic within the environment VPC, use the –internal-alb-allow-vpc-ingress flag; otherwise, access to the internal ALB will be limited by default to only Copilot-created services within the environment.
Need this to allow DCC-created resources to access. Not sure an internal ALB is needed, however.
 
### copilot env deploy
The copilot env deploy will initiate the build of the ECS cluster, private DNS namespace, and the security group.
```
Deploys an environment to an application.

Usage
  copilot env deploy [flags]

Flags
  -a, --app string    Name of the application. (default "mirth")
      --force         Optional. Force update the environment stack template.
  -h, --help          help for deploy
  -n, --name string   Name of the environment.

Examples
Deploy an environment named "test".
`$copilot env deploy --name test`
```

### copilot svc init
Repeat this step for every service in the application.
- Flags
```
-a, --app string Name of the application.
-d, --dockerfile string Path to the Dockerfile.
Mutually exclusive with -i, --image.
-i, --image string The location of an existing Docker image.
Mutually exclusive with -d, --dockerfile.
-n, --name string Name of the service.
--port uint16 The port on which your service listens.
-t, --svc-type string Type of service to create. Must be one of:
"Request-Driven Web Service," "Load Balanced Web Service," "Backend Service," and "Worker Service.".
```
- copilot svc init –image for CNB images
  The –image flag will need to be used to support Cloud-Native Builds. Another pipeline will be created to manage the creation of Cloud Native builds with a resulting container image stored in the ECR container.
- —svc-type
  Either "Backend Service" or "Worker Service" work within ClearDATA with minimal concerns.  The Load-Balanced and Front-end Services create Network or Application load balancers. Those will require some special attention to make compliance.  Until they are compliant we can put a tag on them that protects them from auto-remediation. See the section above about how to add tags when creating the application.  Look for the section "A special Tag" for additional information. The "Worker Service" expects you to use the AWS SQS service.
Modify the svc manifest file
The command "copilot svc init" creates a manifest file in the /copilot/ directory. This file will need to be modified to run your services properly.
This step will be repeated for every service in the application.
- Network Settings
 The network section should be added to all services to ensure they are in the private subnets. By default, only internet-facing services would be put in public subnets. But I would add the following to all service manifest files.
 ```
 network: 
  vpc:
    placement: “private”
```
   
- Environment Variables
  Environment Variables can be declared directly in the manifest, Or you can create a file with variables and reference the file. For additional information, refer to the following: https://aws.github.io/copilot-cli/docs/developing/environment-variables/#how-do-i-add-my-own-environment-variables.
- Secrets
AWS Copilot is like DCC, allowing you to save secrets into AWS [Secrets Manager](https://aws.github.io/copilot-cli/docs/developing/secrets/).
Since we will be reusing secrets we have already created, you will want to read and understand the section "Bring Your Own Secrets."
- Environments
DCC made it challenging to use environments, forcing the customer to redefine all services within each environment completely. Copilot allows the customer to deploy one repo to multiple domains, using the same code and manifests. Copilot will enable you to deploy environments across various regions and accounts.
The [environment section](https://aws.github.io/copilot-cli/docs/concepts/environments/) allows the customer to manage how a service is deployed in those various environments. For more information, reference the following. 
 
- HTTP section
  The HTTP section of the manifest file will be present in services deployed with load-balanced services.
  The HTTP section is critical because it allows us to override the default behavior of Copilot of putting internet-exposed services on the public subnet. DCC, by default, puts all services on the private subnets and then uses the Application Load Balancer to route traffic to the service on the private subnet appropriately.
  Following is an example section of a manifest file.
```     
# Distribute traffic to your service.
HTTP:
# Requests to this path will be forwarded to your service.
# To match all requests you can use the "/" path.
  path: '/'
# You can specify a custom health check path. The default is "/".
# healthcheck: '/'
    healthcheck:
        path: '/'
        success_codes: '200,301'
        healthy_threshold: 3
        unhealthy_threshold: 2
        interval: 15s
        timeout: 10s
        grace_period: 120s
        deregistration_delay: 5s
        stickiness: false
        alias: yelb-demo.datica.com
``` 

- TLS
  This is defined in the NLB section and will specify the version of TLS allowed.
```
# Specify TLS version 1.2 and above.
nlb:
  ssl_policy: ELBSecurityPolicy-TLS13-1-2-2021-06
```
 
- Storage
  A version of the following must be added to the manifest file for containers that connect to EFS for persistent storage.
```
A version of the following must be added to the manifest file for containers that connect to EFS for persistent storage.

storage:
    volumes:
       <volume name>: # The name of the volume.
         path: /data # Required. The path inside the container. For services migrated from CPaaS it is always /data
         read_only: false # Default: true. For migrated CPaaS services it always false.
         efs:
           id: <File System ID> # Required. example "fs-0f6eb90049d78ccb2"
           root_dir: / # Optional. Defaults to "/". Must not be
           # specified if using access points.
           auth:
              iam: ENABLED # Optional. Whether to use IAM authorization when
              # mounting this filesystem.
              # access_point_id: # Optional. The ID of the EFS Access Point
              # Ref: AccessPointE936DE82yelbdb # to use when mounting this filesystem.
              # uid: 999 # Optional. UID for managed EFS access point.
              # gid: 999 # Optional. GID for managed EFS access point. Cannot be specified
              # with `id`, `root_dir`, or `auth`.
```
   
## Copilot svc deploy
When the manifest file has been modified, you can run "copilot svc deploy." Then choose the svc you want to deploy. Sometimes the change you have made will not be noticed, and in that case, you need to force it by using "copilot svc deploy –force."
This step will be completed for each service in the application.
### Submit this code to a Git repository.
Using an SVC repo is great for managing your code allowing for a way to update your code without having to use AWS Copilot.  A pipeline will enable you to change the code stored in the repo and have the change deployed. If you want to take advantage of a pipeline, you will need to use one of the following code repositories, AWS CodeCommit, GitHub, or Atlassian Bitbucket.
```
copilot pipeline init –app
```
Just like DCC you can create a pipeline for your code services that will allow you to make changes to the code in the repo.  Submit those changes to the repo and they will be deployed.  You can dedicate different branches in a repo to an environment so that you can make changes in the development branch, and have it connected to the development environment.  And when you are ready to push to production merge development to the production branch which then pushes your new code to the production environment.  

For more information, reference the following [Copilot documentation](https://aws.github.io/copilot-cli/docs/concepts/pipelines/).
The following is an example command defining a pipeline using GitHub with a Personal Access Token.
```
$ copilot pipeline init
–github-url git@github.com:mbruntink/example-voting-app.git
–github-access-token
–git-branch copilot/part-2
–environments “test”
```
