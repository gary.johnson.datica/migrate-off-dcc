Make sure AWS copilot is version 20 or greater.
on Mac 

```
curl -Lo copilot https://github.com/aws/copilot-cli/releases/latest/download/copilot-darwin && chmod +x copilot && sudo mv copilot /usr/local/bin/copilot && copilot --help
```

Run the following command to set up the application. Change Directory into the environments. And then create the application. App name needs to begin with "cdco-"
```
copilot app init cdco-mirth --resource-tags "customer=<CustomerName>"
```  

Run the following command. You will be prompted for the following.  Select import VPC, then choose the customer VPC.  Slelect all subnets.
```
copilot env init --name prod --container-insights
```  
Verify the manifest file for environments.  We are not configuring load balancers.
```
copilot env deploy --name prod
```
Update the secrets file with new values where needed.  Then use the following command.
Environments need to be defined and deployed before you can use them in the secrets file.
```
copilot secret init --cli-input-yaml secrets.yaml
```
Take and store the secret file in a secure place.  Do not leave it in the repo.
The output from the above command will provide a set of secrets, that will be used in manifest files. Copy and paste for reuse later.
Go back up one directory level and then into the storage directory.
```
cd ../storage
export RepositoryName=$(basename "$(pwd)"))
copilot svc init --name database1-primary
```
Edit the manifest file and add the secret section generated from the copilot secret init command.  Update the CPU and RAM values to properly size the container.
```
copilot svc deploy --name database1-primary --env prod
copilot svc init --name database1-ro
```
Edit the manifest file and add the secret section generated from the copilot secret init command.  Update the CPU and RAM values to properly size the container.
```
copilot svc deploy --name database1-ro --env prod
```
Create a repo in codecommit
```
aws codecommit create-repository --RepositoryName $RepositoryName
git init --initial-branch=prod
git add .
git commit -m "Inital code commit"
git remote add -t prod origin codecommit::us-east-1://${repositoryName}
git push --set-upstream origin prod
copilot pipeline init --name $RepositoryName --url codecommit::us-east-1://${repositoryName} --git-branch prod --pipeline-type Workloads --environments "prod" 
copilot pipeline deploy --name $RepositoryName
```



Go back up one directory level and then into the "connect" directory.
```
cd ../connect
export RepositoryName=$(basename "$(pwd)"))
```

```
copilot svc init --name ${RepositoryName}1
```
Edit the manifest file and add the secret section generated from the copilot secret init command. Update the CPU and RAM values to properly size the container.
```
copilot svc deploy --name ${RepositoryName}1` --env prod
```

```
copilot svc init --name ${RepositoryName}2
```
Edit the manifest file and add the secret section generated from the copilot secret init command.  Update the CPU and RAM values to properly size the container.
```
copilot svc deploy --name ${RepositoryName}2 --env prod
```
Create a repo in codecommit
```
aws codecommit create-repository --RepositoryName $RepositoryName
```
```
git init --initial-branch=prod
git add .
git commit -m "Inital code commit"
git remote add -t prod origin codecommit::us-east-1://${repositoryName}
git push --set-upstream origin prod
copilot pipeline init --name $RepositoryName --url codecommit::us-east-1://${repositoryName} --git-branch prod --pipeline-type Workloads --environments "prod"
copilot pipeline deploy --name $RepositoryName
```


Go back up one directory level and then cd into the proftpd directory.
```
cd ../proftpd
export RepositoryName=$(basename "$(pwd)"))
copilot svc init --name $RepositoryName
```
Edit the manifest file and add the secret section generated from the copilot secret init command.  Update the CPU and RAM values to properly size the container.
```
copilot svc deploy --name $RepositoryName --env prod
```

Create a repo in codecommit
```
aws codecommit create-repository --repository-name $RepositoryName
git init --initial-branch=prod
git add .
git commit -m "Inital code commit"
git remote add -t prod origin codecommit::us-east-1://${repositoryName}
git push --set-upstream origin prod
copilot pipeline init --name $RepositoryName --url codecommit::us-east-1://${repositoryName} --git-branch prod --pipeline-type Workloads --environments "prod" 
copilot pipeline deploy --name $RepositoryName
```

