#!/usr/bin/env bash

accountId=xxxxxxxx
emailaddress=first.last@company.com
profile=default
copilotProfile=copilot

i=0
while [ $i -ne 10 ]
do
        i=$(($i+1))
        aws sts assume-role --role-arn arn:aws:iam::${accountId}:role/copilot-role --role-session-name "${emailaddress}-copilot" --profile ${profile} | jq -r '.Credentials | "aws configure set aws_session_token \(.SessionToken) --profile ${copilotProfile}\naws configure set aws_access_key_id \(.AccessKeyId) --profile ${copilotProfile}\naws configure set aws_secret_access_key \(.SecretAccessKey) --profile ${copilotProfile}\n"' | sh && aws configure set region us-east-1 --profile ${copilotProfile} && export AWS_PROFILE=${copilotProfile}
        echo "This is step $i of 10.  Sleeping for 900 Seconds"
        sleep 900
done
