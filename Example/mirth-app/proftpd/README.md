This repo is designed to use Copilot.  If this is a new install follow these steps.

FYI...  If you have already defined the app in another directory you still need to start with ```copilot app init`` but just point to the previously created.
1. ```copilot app init``` If you would like all resources tagged with special tags.  This where you would define it using -–resource-tags "<string>."  
1. If this is a new application definition then you need to define environments. If you have already defined environments then those environments will be available.
    -  ```copilot env init``` When doing env init you will need to import the VPC.  You will also want to import ACM certs.
    -  ```copilot env deploy``` There is a manifest file that can be updated and used to manage configuration information.  Deploy looks at the manifest file and deploys the environment with the settings.
1. ```copilot svc init --name proftpd```
```copilot svc deploy --name proftpd --env prod```



Following is notes carried over from CPaaS.

# Proftpd for sftp Service 
Installs Proftpd-basic from Ubuntu Bionic Repositories:
* http://packages.ubuntu.com/bionic/proftpd-basic

# Service Files:

The following service files need to be updated in the proftpd directory

* /etc/proftpd/dhparams.pem
* /etc/proftpd/proftpd.conf.in
* /etc/sv/proftpd/create_sftp_user.sh
* /etc/sv/proftpd/run
* /etc/sv/proftpd/sftp.group
* /etc/sv/proftpd/sftp.passwd

# Environment Variables
The following service files need to be added by the pod-api

* PROFTPDSERVERNAME = FQDN of the sftp service

* MASQUERADE_ADDRESS = MasqueradeAddress causes the server to display the network information for the specified IP address or DNS hostname to the client, on the assumption that that IP address or DNS host is acting as a NAT gateway or port forwarder for the server.  See  http://www.proftpd.org/docs/directives/linked/config_ref_MasqueradeAddress.html

# Change Log:
## 2022-07-19
* Move this to a straight Dockerfile build in one go.  
## 2018-11-07
* Creating the sftp-admin group (GID=1000) by default
* Defaulting all files and directories to be in the sftp-admin group
* Updated the create_sftp_user.sh to be safer to use and support the sftp-admin group
