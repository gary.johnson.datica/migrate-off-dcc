#!/bin/bash

IFS= read -d '' -r HELP << EOF

    Usage: bash $0 [OPTION]...

    Options:
        -h,--help               Print this help document
        -u,--username           REQUIRED. The name of the user.
        -p,--password           A password in plaintext. This will be
                                converted to an MD5 Hash.
        -H,--password-hash      The MD5 Hash of a password.

    Examples:
        Create a new user with a random password:
            bash $0 -u user1

        Create a new user with a password in plaintext:
            bash $0 -u user2 -p "MyReallyCoolPassword!"

        Create a new user with a password hash:
            bash $0 -u user3 -H '\$1\$lo0C.nJO\$veRLCLShbFN1V4IW3lVPN1'

EOF

while [ -n "$*" ] ; do
    case "$1" in
        -h|--help)
            echo "$HELP"
            exit 0
            ;;
        -u|--username)
            username="$2"
            shift 2
            ;;
        -p|--password)
            PASSWORD_HASH="$(openssl passwd -1 "$2")"
            shift 2
            ;;
        -H|--password-hash)
            PASSWORD_HASH="$2"
            shift 2
            ;;
        *)
            echo "${HELP}ERROR: unrecognized option: $1"
            exit 1
            ;;
    esac
done

FTPBASEPATH=/data/proftpd
SFTP_GROUP=sftp-admin

: "${username:?"${HELP}ERROR: the -u,--username option is required"}"
: "${PASSWORD_HASH:=$(
    password="$(openssl rand 32 | base64 -w 0 | cut -c 1-32)"
    echo "Using the random password: $password" >> /dev/tty
    openssl passwd -1 "$password"
)}"

if [ "$(egrep "^$username:" "$FTPBASEPATH"/config/sftp.passwd)" != "" ] ; then
    read -p "You are about to update an existing user ($username). Do you want to continue (Yes/No): " answer
    [ "${answer,,}" == "yes" ] || { echo "Exiting without making changes." ; exit 0 ; }
fi

useradd -c "$username" -K UID_MIN=10000 -b "$FTPBASEPATH"/home -s /bin/false -M -N "$username" && {
    mkdir -p -m 775 "$FTPBASEPATH"/home/"$username"
    chown -R "$username":"$SFTP_GROUP" "$FTPBASEPATH"/home/"$username"
}

USERID="$(egrep "^${username}:" /etc/passwd | cut -d ':' -f 3)"
USER_PERM_STRING="${username}:${PASSWORD_HASH}:${USERID}:${USERID}:$(egrep "^${username}:" /etc/passwd | cut -d ':' -f 5-)"

cp "$FTPBASEPATH"/config/sftp.passwd "$FTPBASEPATH"/config/sftp.passwd.bak

sed -En "/^${username}$/ d ; p" "$FTPBASEPATH"/config/sftp.passwd.bak > "$FTPBASEPATH"/config/sftp.passwd

echo "$USER_PERM_STRING" >> "$FTPBASEPATH"/config/sftp.passwd

echo Done

sv hup /etc/sv/proftpd
