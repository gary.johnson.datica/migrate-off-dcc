#!/usr/bin/env python
import argparse
import os.path
import random
import sys
import time

parser = argparse.ArgumentParser()
parser.add_argument('code', help='process exit code', type=int, nargs='?')
parser.add_argument('status', help='process status', type=int, nargs='?')
parser.add_argument('-s', '--state', help='state file', default='.backoff')
parser.add_argument('-m', '--delay-min', help='minimum delay, in seconds', type=int, default=0)
parser.add_argument('-M', '--delay-max', help='maximum delay, in seconds', type=int, default=15)
parser.add_argument('-d', '--delay-jitter', help='delay jitter, in seconds', type=float, default=0.5)
parser.add_argument('-n', '--retries-max', help='maximum number of failed restarts', type=int, default=None)
parser.add_argument('-u', '--update', help='update delay only', action='store_true')
args = parser.parse_args()


def log(message):
    sys.stderr.write(message + '\n')


prog = parser.prog
fail_count = 0
restart_delay = 0.0

# restore state
try:
    if os.path.exists(args.state):
        with open(args.state) as data:
            fields = data.read().split(' ')
            if len(fields) >= 2:
                fail_count, restart_delay = int(fields[0]), float(fields[1])
except Exception as ex:
    log('%s: could not read state file: %s' % (prog, str(ex)))

# update state
if args.code is not None:
    # check exit code and status
    failed = False
    jitter = args.delay_jitter + args.delay_jitter * random.random()
    if args.code == 0:
        # normal exit
        fail_count = 0
        restart_delay = args.delay_min + jitter
    elif args.code > 0:
        # abnormal exit
        failed = True
    else:
        # process was killed; use status to determine abnormal exit
        if args.status is not None and args.status <= 15:
            # normal kill signal
            fail_count = 0
            restart_delay = 0
        else:
            # abnormal signal
            failed = True

    if failed:
        fail_count += 1
        restart_delay += jitter
        restart_delay = min(restart_delay, args.delay_max + jitter)
        restart_delay = max(restart_delay, args.delay_min + jitter)

    # write new state
    try:
        with open(args.state, 'w') as out:
            out.write('%d %f\n' % (fail_count, restart_delay))
    except Exception as ex:
        log('%s: could not update state file: %s' % (prog, str(ex)))

# bail if only updating state
if args.update:
    sys.exit(0)

# check fail count
max_attempts = args.retries_max
if max_attempts and max_attempts > 0 and fail_count > max_attempts:
    log('%s: reached maximum restart attempts (%d)' % (prog, max_attempts))
    sys.exit(1)

# sleep for restart delay, if any
if restart_delay > 0:
    msg = 'succeeded'
    if fail_count > 0:
        msg = 'failed'
    log('%s: command %s; delaying %fs before restarting' % (prog, msg, restart_delay))
    time.sleep(restart_delay)
sys.exit(0)
