#!/usr/bin/env bash

while getopts c:e:p: flag; do
	case "${flag}" in
	c) CustomerName=${OPTARG} ;;
	e) EnvironmentName=${OPTARG} ;;
    p) profile=${OPTARG} ;;
	esac
done

if [[ -z $profile ]]; then
	read -p 'Please provide the name of your AWS CLI Profile:  ' profile
fi

if [[ -z $CustomerName ]]; then
	read -p 'Please provide the name of the Customer this deployment is for:  ' CustomerName
fi

if [[ -z $EnvironmentName ]]; then
	read -p 'Please provide the name of the Name of the environment for this deployment (usually prod):  ' EnvironmentName
fi

appname=cdco-mirth

export AWS_PROFILE=$profile

echo "Following is to verify that you are using the correct profile"
aws sts get-caller-identity 
#read -p "Verify that you are using the ClearDATA-copilot role. (Enter to continue)"

echo "The current directory is $PWD"

#Run the following command to set up the application...  May need this tag if configuring network "cleardata:remediation-disabled=true"
#Change Directory in to the environments. And then create the application.  Replace the name. App name needs to begin with "cdco-"
cd environment
RepositoryName=${appname}-$(basename $(pwd))
DirName=$(basename $(pwd))

copilot app init $appname --resource-tags "customer=${CustomerName}" 
echo
echo "----------->  Starting $DirName section of $appname using repo $RepositoryName. <----------------"
echo "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
#Run the following command. You will be prompted for the following.  Select import VPC, choose the customer VPC.  Slelect all subnets.
copilot env init --name ${EnvironmentName}  --container-insights  --profile $profile
#Verify the manifest file for environments.  We are not configuring load balancers.
copilot env deploy --name ${EnvironmentName}
#Update the secrets file with new values where needed.  Then use the following command.
#Environments need to be defined and deployed before you can use them in the secrets file.
copilot secret init --cli-input-yaml secrets.yml
#Take and store the secrets file in a secure place.  Do not leave in the repo. .gitignore is configured to not copy secrets.yml
#The output from the above command will provide a set of secrets, that will be used in manifest files. Copy and save for reuse later.
#read -p "Copy and save the secrets section for reuse later.  (Enter to continue)"
#Create a repo in codecommit
aws codecommit create-repository --repository-name $RepositoryName
git init --initial-branch=${EnvironmentName}
git add .
git commit -m "Inital code commit"
git remote add -t ${EnvironmentName} origin codecommit::us-east-1://${RepositoryName}
git push --set-upstream origin ${EnvironmentName}
#No need to deploy a pipeline at this time since it does not hold any services or jobs.
#copilot pipeline init --name $DirName --app cdco-mirth --app cdco-mirth --url codecommit::us-east-1://${RepositoryName} --git-branch ${EnvironmentName} --pipeline-type Workloads --environments "${EnvironmentName}" 
#git add .
#git commit -m "Add /copilot directory for pipeline"
#git push
#copilot pipeline deploy --name $DirName

#Go back one directory level and then into the storage directory.
cd ../storage
RepositoryName=${appname}-$(basename $(pwd))
DirName=$(basename $(pwd))
echo
echo "----------->  Starting $DirName section of $appname using repo $RepositoryName. <----------------"
echo "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
copilot app init $appname 
copilot svc init --name database1-primary
#Edit the manifest file and add the secret section generated from the copilot secret init command.  Update the CPU and RAM values to properly size the container.
#read -p "Edit the manifest file and replace the secrets section with the secrets saved previously."

copilot svc deploy --name database1-primary --env ${EnvironmentName}
copilot svc init --name database1-ro
#Edit the manifest file and add the secret section generated from the copilot secret init command.  Update the CPU and RAM values to properly size the container.
#read -p "Edit the manifest file and replace the secrets section with the secrets saved previously."

copilot svc deploy --name database1-ro --env ${EnvironmentName}
#Create a repo in codecommit
aws codecommit create-repository --repository-name $RepositoryName

git init --initial-branch=${EnvironmentName}
git add .
git commit -m "Inital code commit"
git remote add -t ${EnvironmentName} origin codecommit::us-east-1://${RepositoryName}
git push --set-upstream origin ${EnvironmentName}
copilot pipeline init --name $DirName --app cdco-mirth --app cdco-mirth --url codecommit::us-east-1://${RepositoryName} --git-branch ${EnvironmentName} --pipeline-type Workloads --environments "${EnvironmentName}" 
git add .
git commit -m "Add /copilot directory for pipeline"
git push
copilot pipeline deploy --name $DirName

#Go back one directory level and then into the connect directory.
cd ../connect
RepositoryName=${appname}-$(basename $(pwd))
DirName=$(basename $(pwd))
echo
echo "----------->  Starting $DirName section of $appname using repo $RepositoryName. <----------------"
echo "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
copilot app init $appname 
copilot svc init --name ${DirName}1
#Edit the manifest file and add the secret section generated from the copilot secret init command. Update the CPU and RAM values to properly size the container.
#read -p "Edit the manifest file and replace the secrets section with the secrets saved previously."
copilot svc deploy --name ${DirName}1 --env ${EnvironmentName}

copilot svc init --name ${DirName}2
#Edit the manifest file and add the secret section generated from the copilot secret init command.  Update the CPU and RAM values to properly size the container.
#read -p "Edit the manifest file and replace the secrets section with the secrets saved previously."
copilot svc deploy --name ${DirName}2 --env ${EnvironmentName}
#Create a repo in codecommit
aws codecommit create-repository --repository-name $RepositoryName

git init --initial-branch=${EnvironmentName}
git add .
git commit -m "Inital code commit"
git remote add -t ${EnvironmentName} origin codecommit::us-east-1://${RepositoryName}
git push --set-upstream origin ${EnvironmentName}
copilot pipeline init --name $DirName --app cdco-mirth --app cdco-mirth --url codecommit::us-east-1://${RepositoryName} --git-branch ${EnvironmentName} --pipeline-type Workloads --environments "${EnvironmentName}" 
git add .
git commit -m "Add /copilot directory for pipeline"
git push
copilot pipeline deploy --name $DirName

#Go back one directory level and then into the connect directory.
cd ../proftpd
RepositoryName=${appname}-$(basename $(pwd))
DirName=$(basename $(pwd))
echo
echo "----------->  Starting $DirName section of $appname using repo $RepositoryName. <----------------"
echo "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
copilot app init $appname 
copilot svc init --name $DirName
#Edit the manifest file and add the secret section generated from the copilot secret init command.  Update the CPU and RAM values to properly size the container.
#read -p "Edit the manifest file and replace the secrets section with the secrets saved previously."
copilot svc deploy --name $DirName --env ${EnvironmentName}

#Create a repo in codecommit
aws codecommit create-repository --repository-name $RepositoryName
git init --initial-branch=${EnvironmentName}
git add .
git commit -m "Inital code commit"
git remote add -t ${EnvironmentName} origin codecommit::us-east-1://${RepositoryName}
git push --set-upstream origin ${EnvironmentName}
copilot pipeline init --name $DirName --app cdco-mirth --app cdco-mirth --url codecommit::us-east-1://${RepositoryName} --git-branch ${EnvironmentName} --pipeline-type Workloads --environments "${EnvironmentName}" 
git add .
git commit -m "Add /copilot directory for pipeline"
git push
copilot pipeline deploy --name $DirName
