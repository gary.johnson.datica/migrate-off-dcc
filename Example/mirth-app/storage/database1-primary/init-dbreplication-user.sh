#!/bin/bash
# This file is copied here on the container docker-entrypoint-initdb.d/init-dbreplication-db.sh

set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
	CREATE USER $PG_REPLICATOR_USER REPLICATION PASSWORD '$PG_REPLICATOR_PASS';
    CREATE USER $DATABASE_USERNAME LOGIN PASSWORD '$DATABASE_PASSWORD';
	CREATE DATABASE $MIRTHDB;
    GRANT ALL PRIVILEGES ON DATABASE $MIRTHDB TO $DATABASE_USERNAME;
EOSQL

sed -i '/^host\s*all\s*all\s*127.0.0.1\/32/a host    all             all             0.0.0.0/0               md5' $PGDATA/pg_hba.conf   
sed -i '/^host\s*replication\s*all\s*127.0.0.1\/32/a host    replication     '$PG_REPLICATOR_USER'      0.0.0.0/0               md5' $PGDATA/pg_hba.conf 
sed -i 's/^#wal_level = replica/wal_level = hot_standby/g'  $PGDATA/postgresql.conf

