#!/bin/bash
# This file is copied here on the container docker-entrypoint-initdb.d/init-dbreplication-db.sh
set -e

echo $PRIMARY_DB_HOST.$COPILOT_SERVICE_DISCOVERY_ENDPOINT:*:$POSTGRES_DB:$PG_REPLICATOR_USER:$PG_REPLICATOR_PASS > $HOME/.pgpass
chmod 600 $HOME/.pgpass

if [ ! -e $PGDATA ]; then
   echo "The command is ---> pg_basebackup --host=$PRIMARY_DB_HOST.$COPILOT_SERVICE_DISCOVERY_ENDPOINT --dbname=postgresql://$PG_REPLICATOR_USER:$PG_REPLICATOR_PASS@$PRIMARY_DB_HOST.$COPILOT_SERVICE_DISCOVERY_ENDPOINT --checkpoint=fast -D $PGDATA -R --slot=dbrep -C" ;
   pg_basebackup --host=$PRIMARY_DB_HOST.$COPILOT_SERVICE_DISCOVERY_ENDPOINT --checkpoint=fast -D $PGDATA -R --slot=dbrep -C --dbname=postgresql://$PG_REPLICATOR_USER:$PG_REPLICATOR_PASS@$PRIMARY_DB_HOST.$COPILOT_SERVICE_DISCOVERY_ENDPOINT;
fi   

su - postgres --command "/usr/lib/postgresql/12/bin/postgres -D $PGDATA"
