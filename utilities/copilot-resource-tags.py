import boto3
import argparse
import os
import json

DEFAULT_REGION = "us-east-1"

"""
Typical ssm parameter store application json:
{
   "name":"tagging",
   "account":"399606388468",
   "domain":"",
   "domainHostedZoneID":"",
   "version":"1.0",
   "tags":{
      "cleardata:remediation-disabled":"true"
   }
}
"""

CLEARDATA_TAG = {
    "cleardata:remediation-disabled": "true"
}

COPILOT_PATH = "/copilot/applications/"


def main(command_line_args):
    print("\n\n#######################################################")
    print("Copilot Application Config Tagger")
    kwargs = {
        "profile_name": command_line_args.profile,
        "region_name": command_line_args.region
    }

    session = boto3.Session(**kwargs)
    print("Assessing account number: {}\nUsing profile named {}.\nScanning region {} for application configs".format(
        session.client("sts").get_caller_identity()["Account"],
        command_line_args.profile,
        command_line_args.region
    ))
    tag_target_groups(session)


def tag_target_groups(session):
    client = session.client("ssm")

    response = client.get_parameters_by_path(
        Path=COPILOT_PATH,
        Recursive=True,
        WithDecryption=False
    )

    for parameter in response["Parameters"]:
        parameter_name = parameter["Name"]

        if len(parameter_name.split("/")) == 4:
            value = json.loads(parameter["Value"])

            if "tags" in value.keys() and "cleardata:remediation-disabled" in value["tags"].keys():
                print("\n**** Found application config: {} but the cleardata tag is in place. Skipping".format(
                    parameter_name))
                continue

            print("\n**** Found application config: {}".format(parameter_name))
            print("Current value: {}".format(value))

            # proceed = input("Would you like to adjust this parameter? (y/N)").lower()
            #
            # if proceed == "y":

            if "tags" not in value.keys():
                print("Tags are missing from the value configuration.")
                add_tag = input("Would you like to add the cleardata tag? (y/N)").lower()

                if add_tag == "y":
                    value["tags"] = CLEARDATA_TAG
                    response = client.put_parameter(
                        Name=parameter["Name"],
                        Value=json.dumps(value),
                        Overwrite=True,
                    )

                    print("Updated parameter value {}".format(value))

            else:
                existing_tags = value["tags"]

                print("\nTags exist but the cleardata remediation tag is missing.")
                add_tag = input("Would you like to add the cleardata remediation tag? (y/N)").lower()

                if add_tag == "y":
                    value["tags"].update(CLEARDATA_TAG)
                    response = client.put_parameter(
                        Name=parameter["Name"],
                        Value=json.dumps(value),
                        Overwrite=True,
                    )

                    print("\nUpdated parameter {}".format(parameter["Name"]))
                    print("Updated parameter value {}".format(value))

    print("\n**** Copilot application configuration scan completed.\n\n")


if __name__ == "__main__":
    """
    Main kick off of the script
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--profile', default=os.environ.get('AWS_PROFILE', "default"))
    parser.add_argument('-r', '--region', default=os.environ.get('AWS_REGION', DEFAULT_REGION))
    args = parser.parse_args()

    main(command_line_args=args)
